# Git Tasks

This is a simple repo in which practiced git basic concepts.

### Task to create different branches

- **GUI:** go to branches and create a new branch by giving it a name and a commit message.
- **CLI:** Run `git checkout -b <branch name>` to create a branch by the given name `<branch name>`

### Task to create git issues and close them using commit message

- I have created using GitLab GUI. Go `Issues` tab in the side menu. Create a new issue and give some attributes to it like assignee, tag, description etc.
- Mention issue id like `#<issue_id>` in commit to hook it up with the issue tracking.
- To close issue mention `fixes` keyword and issue id in the same commit message like `this commit fixes #1 and #2`

### Task to rebase branches

This is done by commiting two additional commits to `development` branch then rebasing feature branches to match development branch as follows:
```
git checkout development    # move to dev branch
touch new_file1.txt     # create a new file 
git add new_file1.txt   # stage the file 
git commit -m "first commit"    # commit the file with a message
touch new_file2.txt     # repeat as above for a second commit
git add new_file2.txt
git commit -m "second commit"

git checkout feature/feat_1 # move to feature branch
git rebase development  # rebase it to match dev branch
git checkout feature/feat_2 # repeat same as above
git rebase development>`
```
### Task to merge dev branch to main

- **CLI:** On CLI this can be done as follows:
```
git checkout main   # move to main
git merge development   # merge dev branch to main
```
- **GUI:** From GUI go pull request page, request a new request by specifying source and target branches. Then merge the two branches by clicking on merge button on the pull request tracking page.

